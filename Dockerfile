
FROM node:18.16.0 as base

WORKDIR /app

COPY package*.json ./

RUN npm install

FROM base as test

RUN npm run test

FROM base as build

COPY . .

RUN npm run build

FROM nginx:alpine as production

RUN rm -rf /usr/share/nginx/html/*

COPY --from=build /app/build /usr/share/nginx/html

#COPY --from=build /app/dist /usr/share/nginx/html

EXPOSE 3000 80

CMD ["nginx", "-g", "daemon off;"]
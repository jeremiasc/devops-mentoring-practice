import Question from './Quiz';
const nodemailer = require('nodemailer');

async function sendEmail({ playerName, correctAnswers }) {
  try {
    // Configurar el transporte SMTP
    const transporter = nodemailer.createTransport({
        host: 'smtp.ethereal.email',
        port: 587,
        secure: false,
        auth: {
            user: 'dereck.spinka68@ethereal.email',
            pass: '9GTXyQu4bzcyAVnDNE'
        }
    });

    // Configurar el contenido del correo electrónico
    const mailOptions = {
      from: 'rmadolell@gmail.com', // Reemplaza con tu dirección de correo electrónico
      to: 'rmadolell@gmail.com', // Reemplaza con la dirección de correo electrónico del destinatario
      subject: 'Resultados del Quiz',
      text: `Nombre del jugador: ${playerName}\nRespuestas correctas: ${correctAnswers}`,
    };

    // Enviar el correo electrónico
    //const info = await transporter.sendMail(mailOptions);
    console.log('Correo electrónico enviado:', info.messageId);
  } catch (error) {
    console.error('Error al enviar el correo electrónico:', error);
  }
}

module.exports = sendEmail;

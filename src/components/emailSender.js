import axios from 'axios';

const sendNotification = (result) => {
  const webhookUrl = '/services/T058AHJSP7F/B058DG74F6E/cmCStvAquPJTTy4P4GzY5SWa'; // Reemplaza 'TOKEN' con tu webhook URL de Slack

  const message = `¡Nuevo resultado de quiz!\nNombre: ${result.playerName}\nRespuestas correctas: ${result.correctAnswers}`;

  //return axios.post(webhookUrl, { text: message });
};

const emailSender = {
  sendNotification: sendNotification,
};

export default emailSender;
